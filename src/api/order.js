import http from '@/utils/httpRequest'

/**
 * get 请求使用 params: httpRequest.adornParams(param) 传值
 */
export function getorderinfo(params) {
  return http({
    url: http.adornUrl('/oms/order/list'),
    method: 'get',
    params: http.adornParams(params)
  })
}

export function getorderdetail(id) {
    return http({
        url: http.adornUrl('/oms/order/info/'+id),
        method: 'get'
    })
}

export function addorder(params) {
  return http({
    url: http.adornUrl('/oms/order/kuhuxiadanjijian'),
    method: 'get',
    params: http.adornParams(params)
  })
}

export function getgoodtype() {
  return http({
    url: http.adornUrl('/base/goodstype/findgoodstypeAll'),
    method: 'get'
  })
}
export function pickupover(id) {
  return http({
    url: http.adornUrl('/oms/order/dopickup/'+id),
    method: 'get'
  })
}


