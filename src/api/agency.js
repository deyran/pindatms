import http from '@/utils/httpRequest'

/**
 * git 请求使用 params: httpRequest.adornParams(param) 传值
 */
export function getagencyList(param) {
    return httpRequest({
        url: httpRequest.adornUrl('auth/coreorg/list/tree'),
        method: 'get',
        params: httpRequest.adornParams(param)
    })
}

export function savejgfw(params) {
  return http({
    url: http.adornUrl('/auth/coreorg/savejgfw/'+params.jgjd+"/"+params.jgwd+"/"+params.jgzybj+"/"+params.jgbh),
    method: 'get',
  })
}

export function getorderinfokdyxx(params) {
  return http({
    url: http.adornUrl('/auth/authuser/kdylist'),
    method: 'get',
    params: http.adornParams(params)
  })
}
export function getagency() {
  return http({
    url: http.adornUrl('/auth/coreorg/getagency'),
    method: 'get',
  })
}
