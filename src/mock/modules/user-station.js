import Mock from 'mockjs'

// 生成数据列表
var dataList = []
for (let i = 0; i < Math.floor(Math.random() * 10 + 1); i++) {
  dataList.push(Mock.mock({
    'stationId': '@increment',
    'stationName': '@name',
    'createUserId': 1,
  }))
}

export function list () {
  return {
    // isOpen: false,
    url: '/auth/corestation/list',
    type: 'get',
    data: {
      'msg': 'success',
      'code': 0,
      'page': {
        'totalCount': dataList.length,
        'pageSize': 10,
        'totalPage': 1,
        'currPage': 1,
        'list': dataList
      }
    }
  }
}

export function info () {
  return {
    // isOpen: false,
    url: '/auth/corestation/info',
    type: 'get',
    data: {
      'msg': 'success',
      'code': 0,
      'role': dataList[0]
    }
  }
}

export function add () {
  return {
    // isOpen: false,
    url: '/auth/corestation/save',
    type: 'post',
    data: {
      'msg': 'success',
      'code': 0
    }
  }
}


export function update () {
  return {
    // isOpen: false,
    url: '/auth/corestation/update',
    type: 'post',
    data: {
      'msg': 'success',
      'code': 0
    }
  }
}


export function del () {
  return {
    // isOpen: false,
    url: '/auth/corestation/delete',
    type: 'post',
    data: {
      'msg': 'success',
      'code': 0
    }
  }
}
